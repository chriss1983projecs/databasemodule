package com.redeyed.database;

import com.redeyed.database.annotations.Model;
import com.redeyed.database.manager.DatabaseManager;
import com.redeyed.database.model.Player;
import com.redeyed.database.pojos.DataBaseConfig;
import com.redeyed.database.utils.JSONMapper;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;

public class DatabaseTest {

    final static Logger logger = Logger.getLogger(HibernateConfigUtil.class);

    /**
     * JSON Database Settings
     */
    private final String username = "root";
    private final String password = "Ift9h2vC";
    private final String databaseURL = "jdbc:mysql://redeyed.de:3306/test";
    private final String dialect = "MySQLDialect";
    private final String driver = "com.mysql.jdbc.Driver";

    /**
     * Package to search for Entitys
     */
    private final String PACKAGE = "com.redeyed";

    /**
     * Database settings Model
     */
    private DataBaseConfig config;

    /**
     * Set to save the Entitys for HibernateConfigUtil class
     */
    private Set<Class<?>> annotated;

    /**
     * the ID if the player which we whant do modified or delete
     */
    private int playerId;

    /**
     * Entity which we use to test
     */
    private Player playerModel;

    @Before
    public void setUp() {
        String databaseConfig = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"databaseURL\":\"" + databaseURL + "\",\"dialect\":\"" + dialect + "\",\"driver\":\"" + driver + "\"}";
        config = JSONMapper.createDatabaseObject(databaseConfig);
        annotated = new Reflections(PACKAGE).getTypesAnnotatedWith(Model.class);

        RedeyedCoreManager.getDataBaseUtil().initDatabaseConnection(databaseConfig, annotated);

        playerModel  = createPlayerModel();

        DatabaseManager databaseManager = new DatabaseManager();

        ListenerTest t = new ListenerTest();
        databaseManager.addDatabaseListener(t);
    }

    /**
     * Test if JSON Mapping to Object works
     */
    @Test
    public void testJSONMapping() {
        Assert.assertEquals(username, config.getUsername());
        Assert.assertEquals(password, config.getPassword());
        Assert.assertEquals(databaseURL, config.getDatabaseURL());
        Assert.assertEquals(dialect, config.getDialect());
        Assert.assertEquals(driver, config.getDriver());
    }

    @Test
    public void testWriteEntityToDataBase() throws Exception {
        logger.debug("Write object to Database " + playerModel.toString());
        playerId = RedeyedCoreManager.getDataBaseUtil().insertEntity(playerModel);

        /**
         * Test get Player from database
         */
        Player player = RedeyedCoreManager.getDataBaseUtil().getEntity(Player.class, playerId);
        Assert.assertNotNull(player);

        /**
         * Modify databae entity
         */
        logger.debug("Update Entity: change firstName from " + playerModel.getFirstName() + " to Michael ");
        playerModel.setFirstName("Michael");
        Player tmp = (Player) RedeyedCoreManager.getDataBaseUtil().update(playerModel);

        logger.debug("TMP OBJECT = " + tmp);

        Player tmpPlayer = RedeyedCoreManager.getDataBaseUtil().getEntity(Player.class, playerId);
        Assert.assertNotNull(tmpPlayer);
        Assert.assertEquals(tmpPlayer.getFirstName(), "Michael");
        logger.debug("The firstname is now: " + tmpPlayer.getFirstName());
        logger.debug("Get object with " + playerId + " from database: " + playerModel);
        logger.debug("Delete Object from Database");
        Assert.assertTrue(RedeyedCoreManager.getDataBaseUtil().deleteEntity(Player.class, playerId));
    }

    @After
    public void tearDown() throws Exception {
        RedeyedCoreManager.getDataBaseUtil().closeSession();
    }

    private Player createPlayerModel() {
        Player p = new Player();

        p.setFirstName("Peter");
        p.setLastName("Maier");
        p.setCity("Mettmann");
        p.setEMail("christian.richter@ish.de");
        p.setHouseNumber("a56");
        p.setInTeam(false);
        p.setMobilePhoneNumber("015208538917");
        p.setPaid(false);
        p.setPhone("02104/2118973");
        p.setPlayerInfo("This is a smal info for Player, here you can add additional infos");
        p.setStreet("Hasseler Str");
        p.setZipCode("40822");
        return p;
    }
}


