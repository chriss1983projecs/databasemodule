package com.redeyed.database;

import com.redeyed.database.manager.DatabaseListener;
import org.apache.log4j.Logger;

public class ListenerTest implements DatabaseListener {
    final static Logger logger = Logger.getLogger(ListenerTest.class);

    @Override
    public void onSaveOrUpdate(Object entityObject) {
        logger.debug("onSaveOrUpdate");
    }

    @Override
    public void onUpdate(Object playerData) {
        logger.debug("onUpdate");
    }

    @Override
    public void onInsertEntity(Object entity) {
        logger.debug("onInsertEntity");
    }

    @Override
    public void onDeleteEntity(Object object) {
        logger.debug("onDeleteEntity");
    }

    @Override
    public void onDeleteEntity(Class<?> clazz, int entityId) {
        logger.debug("onDeleteEntity");
    }

    @Override
    public void onGetAllEntities() {
        logger.debug("onGetAllEntities");
    }

    @Override
    public <T> void onGetEntity(Class<T> clazz, int entityId) {
        logger.debug("onGetEntity");
    }
}
