package com.redeyed.database.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.redeyed.database.pojos.DataBaseConfig;
import org.apache.log4j.Logger;

public class JSONMapper {
	final static Logger logger = Logger.getLogger(JSONMapper.class);
	private static JSONMapper jsonMapper;
	private static Gson GSON = new GsonBuilder().create();

	public JSONMapper() {
	}

	public DataBaseConfig createObject(String databaseJsonConfig) {
		DataBaseConfig databaseConfig;
		databaseConfig = GSON.fromJson(databaseJsonConfig, DataBaseConfig.class);
		logger.debug(databaseConfig);
		return databaseConfig;
	}

	public static DataBaseConfig createDatabaseObject(String databaseJsonConfig) {
		if (jsonMapper == null) {
			jsonMapper = new JSONMapper();
		}
		return jsonMapper.createObject(databaseJsonConfig);
	}

}
