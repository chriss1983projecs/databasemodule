package com.redeyed.database;

import com.redeyed.database.manager.DatabaseManager;
import com.redeyed.database.pojos.DataBaseConfig;
import com.redeyed.database.query.base.components.GroupBy;
import com.redeyed.database.query.base.components.OrderBy;
import com.redeyed.database.query.base.components.Pagination;
import com.redeyed.database.utils.JSONMapper;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

//"Entry Point Class"
@Component
public class RedeyedCoreManager {
    final static Logger logger = Logger.getLogger(RedeyedCoreManager.class);
    private DatabaseManager databaseManager;
    private static RedeyedCoreManager dataBaseUtil;

    private SessionFactory sessionFactory;

    private Session session;
    private DataBaseConfig baseConfig;
    private Set<Class<?>> classes;

    private RedeyedCoreManager() {
        logger.info("Init DateBaseUtil");
        this.databaseManager = new DatabaseManager();
//		if(baseConfig == null) {
//			System.err.println("Please use first \"initDatabaseConnection(String databaseJsonConfig)\"");
//			System.exit(1);
//		}

    }

    public void initDatabaseConnection(String databaseJsonConfig, Set<Class<?>> classes) {
        baseConfig = JSONMapper.createDatabaseObject(databaseJsonConfig);
        logger.debug("Database Object is: " + baseConfig.toString());
        this.classes = classes;

        sessionFactory = HibernateConfigUtil.getInstance().buildSessionConfigFactory(baseConfig, classes);
//		this.sessionFactory = HibernateConfigUtil.getInstance().getSessionConfigFactory();

        if (sessionFactory == null) {
            logger.debug("SessionFactory is NULL");
        }
        session = sessionFactory.openSession();
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> getAllEntities(Class<T> clazz, List<String> conditions, OrderBy orderBy, Pagination pagination,
                                      GroupBy groupBy) {
        databaseManager.onGetAllEntities();
        try {
            String queryString = "FROM " + clazz.getSimpleName() + getConditions(conditions) + getGroupBy(groupBy)
                    + getOrderBy(orderBy);

            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Query q = session.createQuery(queryString);
            setPagination(q, pagination, queryString);
            return q.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            logger.debug(ex.getMessage());
            return new ArrayList<T>();
        }
    }

    private String getGroupBy(GroupBy groupBy) {
        if (groupBy != null) {
            return " GROUP BY " + groupBy.getVariableName() + " ";
        } else {
            return "";
        }
    }

    private void setPagination(Query query, Pagination pagination, String queryString) {
        if (pagination != null && pagination.getPageNumber() != null && pagination.getPageSize() != null) {
            query.setFirstResult((pagination.getPageNumber() - 1) * pagination.getPageSize());
            query.setMaxResults(pagination.getPageSize());
            pagination.setTotalRows(getRowCount(queryString));
        }
    }

    private String getOrderBy(OrderBy orderBy) {
        if (orderBy != null) {
            return " ORDER BY " + orderBy.getVariableName() + " " + (orderBy.isSortAsc() == true ? "ASC" : "DESC");
        } else {
            return "";
        }
    }

    private String getConditions(List<String> conditions) {
        if (conditions != null) {
            StringBuilder conditionsStr = new StringBuilder(" WHERE ");
            for (String condition : conditions)
                conditionsStr.append(condition).append(" AND ");
            return conditionsStr.substring(0, conditionsStr.length() - 5);
        }
        return "";
    }

    public boolean save(Object object) {
        databaseManager.onSaveOrUpdate(object);
        try {
            session.getTransaction().begin();
            session.save(object);
            session.getTransaction().commit();
        } catch (Exception ex) {
            // logger.error(ex.getCause().getMessage(), ex);
        }
        return true;
    }

    public Object update(Object object) {
        databaseManager.onUpdate(object);
        try {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            session.getTransaction().begin();
            Object updatedObject = session.merge(object);
            session.getTransaction().commit();
            databaseManager.onUpdate(updatedObject);
            return updatedObject;

        } catch (HibernateException e) {
            System.err.println(e.getMessage());
            return null;
        } finally {
            session.close();
        }

    }

    public void updateTeam(/* Team teamData, */Integer teamID) {
        // databaseManager.onUpdatePlayer(teamData, teamID);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // Team team = getEntity(Team.class, teamID);

            // logger.debug("update entity from Database: " + teamData.toString());

            // team.setId(teamData.getId());
            // team.setPointsSeriesOne(teamData.getPointsSeriesOne());
            // team.setPointsSeriesTwo(teamData.getPointsSeriesTwo());
            // team.setTotalScore(teamData.getTotalScore());
            // team.setTeamName(teamData.getTeamName());
            // team.setNewPlayerList(teamData.getPlayers());

            // session.merge(team);
            tx.commit();

        } catch (HibernateException e) {

            if (tx != null)

                tx.rollback();

            e.printStackTrace();
            System.err.println(e.getMessage());
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T getEntity(Class<T> clazz, int entityId) {
        databaseManager.onGetEntity(clazz, entityId);
        try {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }

            session.getTransaction().begin();
            session.get(clazz, entityId);
            session.getTransaction().commit();

            return (T) session.get(clazz, entityId);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
            //return null;
        }
    }

    @Transactional
    public boolean deleteEntity(Class<?> clazz, int entityId) throws Exception {
        databaseManager.onDeleteEntity(clazz, entityId);
        try {
            Object entity = getEntity(clazz, entityId);
            deleteEntity(entity);
            return true;
        } catch (Exception ex) {
//			logger.error(ex.getMessage(), ex);
            logger.debug(ex);
            throw ex;

        }
    }

    @Transactional
    public boolean deleteEntity(Object object) throws Exception {
        databaseManager.onDeleteEntity(object);
        try {
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            logger.debug(ex.getMessage());
            session.getTransaction().rollback();
            return false;
        } finally {
            try {
                HibernateConfigUtil.getInstance().close();
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    public Integer insertEntity(Object entity) {
        databaseManager.onInsertEntity(entity);
        logger.debug("Insert new Entity to Database: " + entity.toString());
        logger.debug("Insert new Entity to Database: " + entity.toString());
        session.getTransaction().begin();
        Integer id = (Integer) session.save(entity);
        session.getTransaction().commit();
        return id;
    }

    private <T> int getRowCount(String queryString) {
        String query = "SELECT COUNT(*) " + queryString;
        return ((Long) this.sessionFactory.getCurrentSession().createQuery(query).uniqueResult()).intValue();
    }

    public <T> int getRowCount(Class<T> clazz) {
        return getRowCount("FROM " + clazz.getSimpleName());
    }

    /*
     * public Boolean checkIfPlayerExists(Player p) { List<Player> players =
     * RedeyedCoreManager.getDataBaseUtil().getAllEntities(Player.class, null, null,
     * null, null);
     *
     * for (Player player : players) { if (p.getId() == player.getId()) { return
     * true; } }
     *
     * return false; }
     */
    public static RedeyedCoreManager getDataBaseUtil() {
        if (dataBaseUtil == null) {
            dataBaseUtil = new RedeyedCoreManager();
        }
        return dataBaseUtil;
    }

    public void shutdown() throws SQLException {
        sessionFactory.close(); // if there are no other open connection
    }

    public void closeSession() {

    }
}
